### 几篇涉及实操的 blogs

- 主要提供了 `reg`，`areg`，`xtreg` 计算 Cluster-SE 时的区别，并提供了实操时的调整的方法。同时，还提供了完整的模拟分析 dofile
  - [Is the file drawer too large? Standard Errors in Stata Strike Back](https://www.fionaburlig.com/blog/2016/8/16/is-the-file-drawer-too-large)
 
#### Stata 官网的 Blogs
  - [How can the standard errors with the vce(cluster clustvar) option be smaller than those without the vce(cluster clustvar) option?](https://www.stata.com/support/faqs/statistics/standard-errors-and-vce-cluster-option/)
当组内干扰项负相关时，clustered-SE 有可能比 robust-SE 小，但这种情况比较少见。Cameron, C. A., D. L. Miller, 2015, A practitioner’s guide to cluster-robust inference, Journal of Human Resources, 50 (2): 317-372. 文中对此有详细介绍。
  - [使用聚类调整标准误时如何标注参考文献？ Which references should I cite when using the vce(cluster clustvar) option to obtain Stata’s cluster-correlated robust estimate of variance?](https://www.stata.com/support/faqs/statistics/references/) 其实，只要标注这篇文章就够了，里面把经典的文献都梳理的很清楚：Cameron, C. A., D. L. Miller, 2015, A practitioner’s guide to cluster-robust inference, Journal of Human Resources, 50 (2): 317-372. 

#### 一些重要的参考文献
- Cameron, C. A., D. L. Miller, 2015, A practitioner’s guide to cluster-robust inference, Journal of Human Resources, 50 (2): 317-372. 非常全面的建议，里面也提供了各种设定下能够使用的 Stata 命令。
- Petersen, M. A., 2009, Estimating standard errors in finance panel data sets: Comparing approaches, Review of Financial Studies, 22 (1): 435-480. A. [Programming Advice - Stata 命令的使用建议](http://www.kellogg.northwestern.edu/faculty/petersen/htm/papers/se/se_programming.htm) ，B. [文中未能呈现的其他表格 - 30页](http://www.kellogg.northwestern.edu/faculty/petersen/htm/papers/standarderror_extra_tables.pdf)，C. [文中模拟过程 - dofile](http://www.kellogg.northwestern.edu/faculty/petersen/htm/papers/se/se_programming.htm#_Simulated_Data_Sets)