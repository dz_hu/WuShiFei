
*-分组计算残差

webuse grunfeld, clear      // 调入数据
des
*-Note: invest - 总投资支出； 
*       mvalue - 前期市场价值； 
*       kstock - 前期固定资产价值

*-2 参数不变模型
reg invest mvalue kstock
predict inv_fit  // invest 的拟合值
predict E0, res   // 残差，需要附加 residual 选项，可以简写为 res
label var E0 "E0"

list comp year inv* E0 if mod(year,5)==0, sep(4)
/*
xtline E0 if comp<=6, yline(0, lc(green) lp(dash))
graph export E0_xtline.png, replace
*/


*-2 分年度变参数模型
*webuse grunfeld, clear

egen t = group(year)  //生成 1,2, T 年份标示变量，防止原始年份数据不连续
sum t
local T = r(max)  //最后一年
gen Et = .  // 用于记录残差的变量

forvalues i=1/`T'{
   qui reg invest mvalue kstock if t==`i' // 分年度回归
   qui predict e_i if e(sample), res  // 第 t 年的残差 
   qui replace Et = e_i if e(sample) // 将第 t 年的残差计入变量 E
   drop e_i
}

*-列示对比结果
/*
cap drop E_diff
gen E_diff = (E0-Et)/E0*100
list comp year inv* E0 Et E_diff if mod(year,5)==0, sep(4)
ttest E_diff=0
ttest E0 = Et
twoway (kdensity E0) (kdensity Et) 
*/

*-与参数不变模型的对比
xtline E0 Et if comp<=6, yline(0, lc(pink*0.6) lp(dash)) 
graph export Et_xtline.png, replace

*-3 asreg 命令

*- 验证 asreg 命令 -- 按年度分组回归取残差
asreg invest mvalue kstock, by(year) fit   
gen Et_as = _residuals
*-对比
list comp year Et* if mod(year,5)==0, sep(4)

*-生成的分组统计量
format _*    %4.3f
format _Nobs %2.0f
list comp year _Nobs  _adjR2  ///  // 样本数，R2-adj
     _b_mvalue _b_kstock      ///  // 变量的系数估计值
     _fitted _residuals       ///  //拟合值和残差
     if mod(year,5)==0, sep(4) noobs
	 
	 
	 
*- `minimum(#)` 选项的使用
. webuse grunfeld, clear 
. set seed 13579 //设定种子值，保证结果可重现
. sample 50      //随机抽取 50% 的观察值
. xtdes
. bysort comp: gen Ni = _N  //每家公司的年数
. tab comp, sort  
/*
 company |      Freq.  
---------+-------------
       4 |         14  
       9 |         12  
      10 |         12  
       5 |         11  
       6 |         10  
       7 |          9  
       8 |          9  
       1 |          8  
       3 |          8  
       2 |          7  
---------+-------------
   Total |        100  
*/ 

asreg invest mvalue kstock, by(comp) min(10) fit 
 
format _res %4.2f
list comp year Ni _res if mod(year,4)==0, sepby(comp)


*-分行业分年度

*webuse "nlswork.dta", clear     // 调入数据
save nlswork.dta, clear

use "nlswork.dta", clear 
asreg wage age hours tenure collgrad married south, fit by(race occupation)


*-面板数据
use "nlswork.dta", clear 

xtdata, fe

*-或
center ....

asreg ....




. webuse grunfeld, clear 
. asreg invest mvalue kstock i.id, by(comp) fit  //不支持因子变量写法

. tab comp, gen(dumi)
. asreg invest mvalue kstock dumi*, by(comp) fit
