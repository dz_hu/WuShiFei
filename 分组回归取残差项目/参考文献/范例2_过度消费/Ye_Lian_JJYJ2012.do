*                                                       
*        ==========================================     
*        ==========================================     
*                                                       
*                   Stata 学术论文专题                  
*                                                       
*        ==========================================     
*        ==========================================     
*                                                       
*                                                       
*                                                       
*             主讲人：连玉君 副教授                    
*                                                       
*        单  位：中山大学岭南学院金融系                
*        电  邮: arlionn@163.com                        
*        主  页：http://dwz.cn/lianyj                   
*        博  客: http://blog.cnfol.com/arlion           
*        微  博：http://weibo.com/arlionn               
*        课  程：http://www.peixun.net/author/3.html    
*        微  信：lianyj45                               
*                                                       

*------------------------------------------
*------------------------------------------
* 叶德珠,连玉君,黄有光,李东辉.            
*    "消费文化、认知偏差与消费行为偏差".  
*    经济研究, 2012(2): 80-92.             
*------------------------------------------
*------------------------------------------

*-原始论文
  global LIAN "$path\Ye_Lian_JJYJ2012"  //存储该文文件的文件夹
  shellout "$LIAN\连玉君_2012_消费文化.pdf"     //原文

  
  cd "$LIAN"   // 进入课程目录
  /*
  cd "D:\stata13\ado\personal\PX_DUFE\A1_intro\Ye_Lian_JJYJ2012"
  */  
   
*----------- 
*-数据处理
*-----------

   use T_consum_sex_data.dta, clear    // 调入数据
 
   order id year
   tsset id year                       // 声明为面板

 *---------  
 *-变量标签 (多余的变量我未删除)

   label var	id			"国家名称"
   label var	domsaving	"国内储蓄率"	
   label var	consume		"最终消费率"
   label var	netsaving	"净国民储蓄占gdp比例"
   label var	srate		"一年期储蓄利率"
   label var	gdpper		"人均收入"
   label var	ruralpop	"郊区人口"	
   label var	edu			"大学入学率"
   label var	infla		"通胀率"	
   label var	ss			"社会保障"
   label var	ss2	
   label var	insur8		"保险"
   label var	fd1			"金融发展"
   label var	sex1		"性生活指数"
   label var	sex2		"最早性生活的年龄"
   label var	sex3		"进行性教育的年龄"
   label var	sex4		"进行不安全性行为的比例"
   label var	sex5		"每年性生活次数"
   label var	rategap		"长短期利率差"
   label var	tour2		"旅游支出占比"	
   label var	tour3	
   label var	tour1	
   label var	tour4
   rename sex1  sex_index   // 部分变量更名
   rename sex2  sex_age
   rename sex3  sex_EDU_age
   rename sex4  sex_unsafe
   rename sex5  sex_feq
   label var	bankloan	"银行部门提供的国内信贷除以GDP"
   label var	privateloan "对私营部门提供的国内信贷除以GDP"
   label var	ss_new		"社会保障支出占GDP比例"
   label var	old_dep		"老年人负担系数"
   label var	young_dep	"儿童负担系数"
   
   
 *-改变部分变量的单位, 以便估计系数显示在正常范围内(0.00-10.00)
 
   local w1 "domsaving netsaving srate edu infla ss ss2"
   local w2 "insur8 fd1 sex_unsafe rategap old young"
   foreach v of varlist `w1' `w2'{
     replace `v' = `v'/100
   }
   replace bankloan =  bankloan/10000
   replace privateloan = privateloan/10000
   
   clonevar sexfeq0 = sex_feq  // 备份一个变量，绘图时使用 (see Figure 1)
   
   replace sex_feq = sex_feq/360
   label var sex_feq  "每年性生活次数/360"
   
   *-Note: 对变量执行放大或缩小处理，并不影响统计推断的结论，
   *       只是影响系数估计值的大小而已.

   
 *-儒家文化虚拟变量
 
   labelbook id
   gen rujia = inlist(id,7,17,21,25,26,28,34,39,44,48)
   *-中国、香港、印度尼西亚、日本、韩国、马来西亚、菲律宾、新加坡、泰国和越南
   
   
 *-处理离群值，winsor 处理
 
   *-原始变量的分布特征
     des srate infla ss
     sum srate infla ss
 
   local vv "srate infla ss*"  // 需要 winsor 的变量都可以加入此处
   foreach v of varlist `vv'{
     local a: var label `v'
	 if ("`v'"=="infla"|"`v'"=="srate"){
	    winsor `v', p(0.02) gen(`v'_x)
	 }
	 else{
	    winsor `v', p(0.01) gen(`v'_x)
	 }
	 drop `v'
	 rename `v'_x `v'
	 label var `v' "`a'"
   } 
   
   *-winsor 前的分布特征
     des bankloan privateloan
     sum bankloan privateloan
     histogram bankloan
   *-winsor 处理
     winsor bankloan, gen(bank_w) p(0.05) high
     winsor privateloan, gen(private_w) p(0.05) high
   *-winsor 后的分布特征
     sum bank_w private_w
	 histogram bank_w
   
  *-新变量
    gen real_i0 = srate - infla         // 用实质利率会更好一些
    winsor real_i0, gen(real_i) p(0.01)
    gen depend = young_dep + old_dep    // 二者之间相关系数过高，所以合并起来
   
  *-处理部分缺漏值 
	bysort id: egen av_insur8 = mean(insur8)  // 缺漏值用每个国家的样本均值代替
	replace insur8 = av_insur8 if insur8==.   
 
 
 *==========
 *-Table 1 : 基本统计量
 *========== 
 
   global x  "gdpper edu real_i rategap ss depend bank_w" 
   reg consume $x
   ereturn list         // 返回值
   gen yes = e(sample)  // 只统计参与回归分析的观察值
   global v1 "netsaving domsaving consume"
   global v2 "gdpper edu real_i rategap ss depend bank_w"
   global v3 "rujia sex_feq sex_unsafe sex_age"
   global v $v1 $v2 $v3
   
   *-输出到 Excel
     logout, save("Results/Var_statistic") excel replace:  ///
             tabstat $v if yes==1,       ///
			 s(mean p50 sd min max N)    ///
			 format(%6.3f) column(statis)	 
  
  
 *------------------- 
 *-变量的相关系数矩阵 (not reported in the paper)
 *------------------- 

   format $v %6.2f
   logout, save("Results/Var_corr") excel replace:  ///
           pwcorr $v if yes, star(.05)
  
 *------------- 
 *-基本统计分析 (not reported in the paper)
 *------------- 
 
   *-分别统计rujia和非rujia地区的消费率，性生活指数数据，做基本的统计分析
 
    *-分儒家和非儒家两组统计
      tabstat sex* *sav* consume, by(rujia) f(%4.2f)
	
	*-分年度统计
      foreach t of numlist 1978 1980(5)2005 2007{
         display in w _n _n "Year: " `t'
         tabstat sex* *sav*  if year==`t',  /// 
               by(rujia) format(%4.1f) nototal
      }
	

	
 *==========
 *-Table 2 : 推论1检验结果：自我控制认知偏差对最终消费率的影响
 *==========
 
     dropvars yr* fix*   
   
   *-国家个体效应虚拟变量
     tab id, gen(fix)   
     drop fix1
   
   *-年度效应虚拟变量
     tab year, gen(yr)
     drop yr1
   
   *-设定被解释变量和解释变量
     global y  "consume"     // 被解释变量——最终消费率  (文中使用了这个)
     global x  "gdpper real_i rategap ss depend bank_w" 
   
   *-column (1)
     reg $y $x
     est store c1            // OLS
   
   *-column (2)
     reg $y $x fix*
     est store c2            // FE
	 ereturn list            // 呈现返回值，用于随后输出结果
     *-检验个体效应          (p.86, 脚注 1)
       lrtest c1 c2 
	   estadd scalar FeChi2 = r(chi2)
	   estadd scalar FeChi2p= r(p)
	   ereturn list          // 呈现返回值
	   
   *-column (3)	   
     reg $y $x fix* yr*
     est store c3            // FE+Year
     *-检验年度效应             (p.86, 脚注 2)
       lrtest c2 c3	
	   estadd scalar YrChi2 = r(chi2)
	   estadd scalar YrChi2p= r(p)	 
	   
   *-column (4)	     
     global z "rujia"        
     reg $y $x $z 
     est store c4            // OLS+rujia 
	 
   *-column (5)	   
     global z "sex_feq"
     reg $y $x $z  
     est store c5            // OLS+sex_feq
	 
	 *-用第 (5) 栏的样本重新估计第 (4) 栏的模型   (p.87, 脚注 2)
	 reg $y $x rujia if e(sample)
	 dis "adj-R2 = " e(r2_a)
   
   *-column (6)	    
     global z "sex_unsafe"
     reg $y $x $z  
     est store c6	 
   
   *-column (7)	    
     global z "sex_age"   
     reg $y $x $z 
     est store c7
   
   *-column (8)	    
     global z "rujia sex_feq"   
     reg $y $x $z  
     est store c8

	 
   *-呈现结果
     global mm "c1 c2 c3 c4 c5 c6 c7 c8"
     dis "Denpendent = " in y "$y"
     esttab $mm, b(%6.3f) star(* 0.1 ** 0.05 *** 0.01)  ///
         scalar(r2 r2_a F N FeChi2 FeChi2p YrChi2 YrChi2p)  ///
		 compress nogaps          ///
         order($x) drop(*fix* yr*) ///
		 mtitle(OLS FE FE+Year OLS OLS OLS OLS OLS)

   *-输出到 Excel
     esttab $mm using "Results/Table02_$y.csv", replace   ///
	     b(%6.3f) star(* 0.1 ** 0.05 *** 0.01)     ///
         scalar(r2 r2_a F N FeChi2 FeChi2p YrChi2 YrChi2p)  ///
		 nogaps order($x) drop(*fix* yr*)   ///
		 mtitle(OLS FE FE+Year OLS OLS OLS OLS OLS)

		 
 *------------		 
 *-稳健性检验	 (not reported in the final version of the paper)
 *------------
 
  *-分别采用 domsaving 和 netsaving 做为被解释变量
  
  *----------
  *-domsaving
  
    global y  "domsaving"  // 被解释变量 1 国内储蓄率
   *global x  "gdpper real_i rategap ss depend bank_w" 
   
    reg $y $x
      est store rd0    
    
	reg $y $x fix*
      est store rd1       
    
	global z "rujia"        
    reg $y $x $z 
      est store rd2        
    
	global z "sex_feq"
    reg $y $x $z 
      est store rd3        

  *----------
  *-netsaving	
  
    global y  "netsaving"   // 被解释变量2 净储蓄率  
    
	reg $y $x
      est store rn0   
    
	reg $y $x fix*
      est store rn1    
    
	global z "rujia"        
    reg $y $x $z 
      est store rn2  
    
	global z "sex_feq"
    reg $y $x $z  
      est store rn3 
	 
   *-呈现结果
     global mm "rd0 rd1 rd2 rd3  rn0 rn1 rn2 rn3"
     esttab $mm, b(%6.3f) star(* 0.1 ** 0.05 *** 0.01)  ///
         scalar(N r2_a) compress nogaps          ///
         order($x) drop(*fix*) ///
		 mtitle(OLS FE domsave OLS OLS FE netsave OLS)

   *-输出到 Excel
     esttab $mm using "Results/Table02_Robust.csv", replace   ///
	     b(%6.3f) star(* 0.1 ** 0.05 *** 0.01)     ///
         scalar(N r2_a) nogaps  ///
		 order($x) drop(*fix*)   ///
		 mtitle(OLS FE domsave OLS OLS FE netsave OLS)
  
	
	
 *==========
 *-Table 3 : 推论2的检验结果：异常消费与理性消费者占比
 *==========	
 
     global y  "domsaving"  // 被解释变量1 国内储蓄率
   * global y  "netsaving"  // 被解释变量2 净储蓄率
   * global y  "consume"    // 被解释变量3 最终消费率

 *--------	 
 *-第一步: 计算离差 (异常消费和异常性行为)
 *--------
 
   *-y 的离差：全样本 OLS 回归法
   
	 global x  "gdpper real_i ss depend bank_w"
	 dropvars Ex_$y Exdum abs_Ex_$y 
	 foreach v of varlist consume domsaving netsaving{
	   qui reg `v' $x 
	   qui predict Ex_`v' if e(sample), res
	   qui gen abs_Ex_`v' = abs(Ex_`v')  // Ex_$y 的绝对值  
	 }
	 	 
	 gen Exdum = (Ex_$y>0)
	 replace Exdum=. if Ex_$y==.
	 
	 tabstat sex* *sav* consume, by(Exdum) f(%4.2f)
	 tabstat sex* *sav* consume, by(rujia) f(%4.2f)
	  
	  
   *-sex_index 的离差
   
	 dropvars av_sex* Ex_sex*
	 foreach v of varlist sex_feq sex_age sex_unsafe{
	   bysort year: egen av_`v' = mean(`v')
	   gen Ex_`v' = abs(`v' - av_`v')
	 }
	 
	
 *--------	 
 *-第二步: 回归分析 (表 3)
 *--------	

	 dropvars Ex_sexfeq_x_edu 
	 gen Ex_sexfeq_x_edu = Ex_sex_feq*edu
 
     local y "consume"
	 reg abs_Ex_`y' Ex_sex_feq 
	   est store a1
	 reg abs_Ex_`y' edu
	   est store a2
	 reg abs_Ex_`y' edu Ex_sex_feq
	   est store a3	 
	   
     local y "domsaving"
	 reg abs_Ex_`y' Ex_sex_feq 
	   est store b1
	 reg abs_Ex_`y' edu
	   est store b2
	 reg abs_Ex_`y' edu Ex_sex_feq
	   est store b3		   

     local y "netsaving"
	 reg abs_Ex_`y' Ex_sex_feq 
	   est store c1
	 reg abs_Ex_`y' edu
	   est store c2
	 reg abs_Ex_`y' edu Ex_sex_feq
	   est store c3		
	 
   *-呈现结果
   
    *-Panel A
      local mm "a1 a2 a3"  
      esttab `mm', mtitle("Ex_Consume") b(%6.3f) nogaps ///
	        star(* 0.1 ** 0.05 *** 0.01) scalar(r2 r2_a N)
            
    *-Panel B   
      local mm "b1 b2 b3" 
      esttab `mm', mtitle("Ex_Domsaving") b(%6.3f) nogaps ///
	        star(* 0.1 ** 0.05 *** 0.01) scalar(r2 r2_a N)
            
    *-Panel C	   
      local mm "c1 c2 c3" 
      esttab `mm', mtitle("Ex_Netsaving") b(%6.3f) nogaps ///
	        star(* 0.1 ** 0.05 *** 0.01) scalar(r2 r2_a N)
 			
			
   *-输出到 Excel
     local mm "a1 a2 a3"  
     esttab `mm' using "Results/Table03_Ex.csv",    ///
	        star(* 0.1 ** 0.05 *** 0.01) b(%6.3f) ///
            scalar(N r2_a) wide nogaps replace	
			
     local mm "b1 b2 b3"  
     esttab `mm' using "Results/Table03_Ex.csv",    ///
	        star(* 0.1 ** 0.05 *** 0.01) b(%6.3f) ///
            scalar(N r2_a) wide nogaps append     // 追加结果到上一张表中	
			
     local mm "c1 c2 c3"  
     esttab `mm' using "Results/Table03_Ex.csv",    ///
	        star(* 0.1 ** 0.05 *** 0.01) b(%6.3f) ///
            scalar(N r2_a) wide nogaps append     // 追加结果到上一张表中	
 
 
 *==========
 *-Figure 00  Discounting function  Eq.(1) (not reported)
 *==========
 
 *---------------------------------Fig.00------begin---------------
  preserve
    clear
	set obs 10
	local delta = 0.8
	gen t = _n - 1
	gen dt = `delta'^t
	gen sum = sum(dt)
	local b = 0.4    // beta<1
	  gen yn1 = 1 
	  replace yn1 = `b'*`delta'^t if t>0
	local b = 1      // beta=1
	  gen ye1 = 1 
	  replace ye1 = `b'*`delta'^t if t>0
	local b = 1.5    // beta>1
	  gen yg1 = 1 
	  replace yg1 = `b'*`delta'^t if t>0	
	#delimit ;  
	twoway (line yn1 t,lw(*1.9))
	       (line ye1 t,lw(*1.9))
		   (line yg1 t,lw(*1.9))
		   (dropline yg1 t if t<9, lstyle(grid) msymbol(none))
	       , 
		   scheme(s1mono) 
	       ytitle("贴" "现" "函" "数", place(top) 
		          orientation(horizontal) size(*1.3)) 
	       xtitle("{it:t}", size(*1.5) place(right)) 
	       ylabel(1, angle(0) labsize(*1.3)) 
		   xlabel(1(1)8, labsize(*1.3))
		   yscale(range(0 1.4) titlegap(-4))
	       xscale(titlegap(-4) outergap(-3))
		   plotregion(style(none) margin(0))
		   legend(off)
		   text(0.99 2.5 "{it:{&beta}} >1", size(*1.3))
		   text(0.68 2.5 "{it:{&beta}} =1", size(*1.3))
		   text(0.31 2.5 "{it:{&beta}} <1", size(*1.3))
	       ;
	 #delimit cr
	 graph export "Results/Fig00_discount.wmf", replace  // 保存图片 
  restore
 *---------------------------------Fig.00------over----------------   
 
	 
 *==========
 *-Figure 01  图1 消费偏差与每年性生活次数的散点图（1998-2007）
 *========== 
   
 *-部分国家名称简写
   global y "consume"
   label define id  ///
          2 "AU"    ///   // Austalia  
		  3 "AT"    ///   // Austia
		  4 "BE"    ///   // Belgium
          7 "China" ///
         10 "Crech" ///
		 11 "DK"    ///   // Denmark
		 13 "FI"    ///   // Finland
		 15 "DE"    ///   // Germany
		 40 "ZA"    ///   // South Africa
		 41 "ES"    ///   // Spain
		 46 "UK"    ///   // United Kingdom
		 47 "USA", modify
	
     dropvars pos
	 gen pos = 3
	 replace pos = 2  if inlist(id,2,21,39,46,47)
	 replace pos = 4  if inlist(id,6,13,41,45)
	 replace pos = 6  if inlist(id,35,44)
	 replace pos = 9  if inlist(id,15,16,23,24,48)
	 replace pos = 10 if inlist(id,11,19,36)
     replace pos = 12 if inlist(id,40)
	 
	 dropvars av_Exy av_sexfeq
	 bysort id: egen av_Exy = mean(Ex_$y)
	 bysort id: egen av_sexfeq = mean(sexfeq0)
     #delimit ;
     twoway (scatter av_Exy av_sexfeq 
	          if rujia==1, 
			  mcolor(black) msymbol(0) mlabel(id) mlabvp(pos)
			)
	        (scatter av_Exy av_sexfeq
			  if rujia==0&Ex_$y<=0.2,
			  mcolor(black*0.6) msymbol(Th) mlabel(id) mlabvp(pos) mlabsize(small)
			)
			,
			ytitle("与正常消费比率的偏离", size(*0.9))
			yscale(titlegap(1) outergap(0)) 
			ylabel(, angle(0) format(%3.1f))
			xtitle("每年性生活次数", size(*0.9))
			xscale(titlegap(3) outergap(-2)) 
			yline(0, lp(dash) lc(black*0.4))
			legend(label(1 "儒家") label(2 "非儒家") size(*0.8) pos(5) ring(0))
			scheme(s1mono) ;
	 #delimit cr
	 graph export "Results/Fig02_Ex_sex.wmf", replace  // 保存图片
	 
	 
*-----------------------------------Over--------------------------------------


*---------------------------
*-附录: 外审意见及修改说明
*---------------------------

  shellout "外审意见及修改说明.pdf"
	

*==========================================================
*-2011-06-06 更新说明

*-增加了几个新变量
*-重新设定 Normal Consumption 模型
*-重新估计 Excess Consumption 的决定因素

*-2011-06-23 更新说明
*-将 edu 从 Normal Regression 中去掉
*-残差的绝对值衡量异常消费

*-2011-12-13
*-作为稳健性检验，可以采用旅游支出作为过度消费的替代指标。
*==========================================================

