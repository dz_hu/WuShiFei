> 世飞：
- 我把写作建议贴过来了，供你写作时参考。正式写作请在我建立的另一个 `.md` 文档中写。
- 结构安排你自己酌情调整。
- 另外，建议尽可能多一点应用介绍方面的描述，让读者知道这个方法到底有何用。
- 引言部分要花点心思，让读者在尽可能短的时间内了解该推文的主旨。


`2017/11/10 8:46`


## 写在前面

在很多领域，常常采用分组回归得到的残差作为某些重要变量衡量指标。这些指标是后续分析的被解释变量。

例如，

- 在会计和公司金融领域，研究`盈余管理`或`盈余质量`时，便是基于「分行业-分年度」回归得到的残差作为`异常盈余`的指标；如，Roychowdhury (2006)
- 在`事件研究法`中，我们针对每家公司的日交易资料来估计`市场模型`(CAPM 的一个简化版本)，并取其残差用以衡量`异常收益率 (Abnormal Return, AR)`；
- 在公司`投资行为`的研究中，用实际投资支出与可能影响投资行为的变量进行回归，得到的残差往往被视为`非正常投资`，残差为负者视为**投资不足**，为正者则视为**过度投资**
- 在`个体消费`行为的研究中，也会采取相同的思路，估计消费率方程后，用残差来衡量**异常消费**。

类似的例子和应用还有很多。

---
**另外，** 若是能在 Stata 内部，使用 `findit` 搜索到可以直接分组估计残差的命令就更好了。或者我们可以考虑写一个这样的程序，在另一篇推文中发布这个程序。基本的语法结构长这样：
```stata
byres y x, group(industry year) gen(newvarname)
```
- 说明：
  - 程序的名称定义为 `byres`；
  - `group()` 选项用来设定基于哪些变量分组；
  - `gen()` 选项用于让用户指定一个新变量的名称，用于存储新生成的变量。
----


### 参考文献：

##### **盈余管理**
- Dechow, P. M., Sloan, R. G., Sweeney, A. P. Detecting earnings management [J]. The Accounting Review, 1995, 70 (2): 193-225.
- Dechow, P. M., Hutton, A. P., Kim, J. H., Sloan, R. G. Detecting Earnings Management: A New Approach [J]. Journal of Accounting Research, 2012, 50 (2): 275-334.
- Jones, J. J. Earnings management during import relief investigations [J]. Journal of Accounting Research, 1991, 29 (2): 193-228.
- Roychowdhury, S. Earnings management through real activities manipulation [J]. Journal of Accounting and Economics, 2006, 42 (3): 335-370.

##### Event Study (你可以自己查询几篇国内的比较经典的文献)
- Binder, J. The event study methodology since 1969 [J]. Review of Quantitative Finance and Accounting, 1998, 11 (2): 111-137.

##### 过度投资 (你来补充国内引用该文的比较经典的文献，如，张功富等)
- Richardson, S. Over-investment of free cash flow [J]. Review of Accounting Studies, 2006, 11 (2): 159-189.

##### 过度消费
- 叶德珠, 连玉君, 黄有光, 李东辉. 消费文化、认知偏差与消费行为偏差 [J]. 经济研究, 2012, (2): 80-92.

## Stata 范例

下面举两个例子来说明分组获取残差在实证分析中的具体应用。

### Stata 范例 1：Jones 模型 (盈余管理)

该Stata 范例的数据和完整实现过程已经放在了【参考文献】文件夹中。

### Stata 范例 2：性生活次数与过度消费

该 Stata 范例的数据和完整实现过程已经放在了【参考文献】文件夹中。你可以从中抽取有关残差计算的例子。数据可以从我们的数据中提取一个子样本，做成Stata 范例数据。
- 存放时可以新建一个文件夹，存储这个范例的所有文件。
- `叶德珠, 连玉君, 黄有光, 李东辉. 消费文化、认知偏差与消费行为偏差 [J]. 经济研究, 2012, (2): 80-92.`